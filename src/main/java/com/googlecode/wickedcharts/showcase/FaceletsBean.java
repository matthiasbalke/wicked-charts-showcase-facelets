package com.googlecode.wickedcharts.showcase;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.googlecode.wickedcharts.highcharts.options.Options;
import com.googlecode.wickedcharts.showcase.charts.BasicAreaOptions;

@ManagedBean(name = "chart")
@SessionScoped
public class FaceletsBean {

	private Options options = null;

	public Options getOptions() {
		return this.options;
	}

	public FaceletsBean() {
		this.options = new BasicAreaOptions();
	}
}
